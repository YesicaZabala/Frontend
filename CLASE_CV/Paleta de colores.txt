/* CSS HEX */
--dark-purple: #210b2cff;
--spanish-violet: #55286fff;
--wisteria: #bc96e6ff;
--pink-lavender: #d8b4e2ff;
--pearly-purple: #ae759fff;

/* CSS HSL */
--dark-purple: hsla(280, 60%, 11%, 1);
--spanish-violet: hsla(278, 47%, 30%, 1);
--wisteria: hsla(268, 62%, 75%, 1);
--pink-lavender: hsla(287, 44%, 80%, 1);
--pearly-purple: hsla(316, 26%, 57%, 1);

/* SCSS HEX */
$dark-purple: #210b2cff;
$spanish-violet: #55286fff;
$wisteria: #bc96e6ff;
$pink-lavender: #d8b4e2ff;
$pearly-purple: #ae759fff;

/* SCSS HSL */
$dark-purple: hsla(280, 60%, 11%, 1);
$spanish-violet: hsla(278, 47%, 30%, 1);
$wisteria: hsla(268, 62%, 75%, 1);
$pink-lavender: hsla(287, 44%, 80%, 1);
$pearly-purple: hsla(316, 26%, 57%, 1);

/* SCSS RGB */
$dark-purple: rgba(33, 11, 44, 1);
$spanish-violet: rgba(85, 40, 111, 1);
$wisteria: rgba(188, 150, 230, 1);
$pink-lavender: rgba(216, 180, 226, 1);
$pearly-purple: rgba(174, 117, 159, 1);

/* SCSS Gradient */
$gradient-top: linear-gradient(0deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-right: linear-gradient(90deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-bottom: linear-gradient(180deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-left: linear-gradient(270deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-top-right: linear-gradient(45deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-bottom-right: linear-gradient(135deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-top-left: linear-gradient(225deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-bottom-left: linear-gradient(315deg, #210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);
$gradient-radial: radial-gradient(#210b2cff, #55286fff, #bc96e6ff, #d8b4e2ff, #ae759fff);